///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - Crazy Cat Guy - EE 205 - Spr 2022
///
/// @file    crazyCatGuy.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o crazyCatGuy crazyCatGuy.c
///
/// Usage:  crazyCatGuy n
///   n:  Sum the digits from 1 to n
///
/// Result:
///   The sum of the digits from 1 to n is XX
///
/// Example:
///   $ summation 6
///   The sum of the digits from 1 to 6 is 21
///
/// @author  Reid Lum <reidlum@hawaii.edu>
/// @date    11 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>

int main( int argc, char* argv[] ) {
   int n = atoi( argv[1] );
   int sum = 0;
   for (int  i = 1; n >= 1; i++){
      sum = sum + i;
      n = n - 1;
   }
   printf("%d\n", sum);
   return 0;
}
